# Vim Expert

Esse projeto consiste em aprender o Vim da melhor maneira possível e fazer anotações sobre ele. Eu também estou aprendendo o Vim enquanto faço esse repo. É o meu modo de fixar o conhecimento e disponibilizá-lo para a comunidade que tanto me proporcionou oportunidade.

Uma das coisas mais importantes na minha leitura sobre o Vim é humanamente impossível aprender tudo de uma vez. Por isso escreverei esse repositório devagar de acordo com o que vou incluindo o aprendizado no meu workflow.

O Vim é um editor de texto altamente poderoso e muito usado pelos sysadmins, devops, desenvolvedores, etc.
Costuma aparecer como o comando vi na maioria das distribuções baseadas em unix.

- árvore de desfazer persistente e multinível
- extenso sistema de plugins
- suporte para centenas de linguagens de programação e formatos de arquivo
- poderosa pesquisa e substituição
- integra-se com muitas ferramentas
- mantém a mão no teclado aumentando a produtividade a longo prazo
  - A curto prazo terá um atrasado até aprender, mas depois é só alegria
- alta customização
- vem pre-instalado na maioria das distros linux.

## Documentação oficial

<https://www.vim.org/>
<https://www.vim.org/docs.php>
<https://vimhelp.org>

## Instalação

<https://www.vim.org/download.php>

Não perde o seu tempo tentando fazer download não pois o Vim esta disponível em praticamente todas as distros linux. Então vai no seu instalador de pacote manda instalar que já está lá, acredite!

baseados em debian

````bash
sudo apt-get install vim
````

baseados red hat

````bash
sudo yum install vim-enhanced
````

arch linux

````bash
sudo pacman -S vim
````

no mac pode ser instalado pelo homebrew

````bash
brew install vim
````

no windows vc pode baixar o executavel ou instalar pelo chocolatey

````bash
choco install vim
````

Se quiser compilar fique a vontade:

````bash
git clone https://github.com/vim/vim.git
cd vim/src
make
````

## Neo Vim

Existe um fork do vim muito utilizado pela comunidade que é o [NeoVim](https://neovim.io/). Este é uma atualização aprimorada do Vim, com mais recursos. Hoje em dia acho que vale mais a pena partir para o NeoVim.

>O NeoVim é um fork do Vim que aborda alguns dos principais problemas do Vim. Um dos principais objetivos do NeoVim é construir um projeto de código aberto voltado para a comunidade.

Em termos de arquitetura, o NeoVim é projetado de forma mais eficiente e sustentável. Portanto, a natureza sustentável do NeoVim reduz ligeiramente a barreira de entrada para qualquer pessoa interessada em contribuir. Como resultado disso, o NeoVim cresce como software muito mais rápido.

- API de plugin do NeoVim é melhor e incorpora a linguagem Lua.
- Plugins mais poderosos que não são compatíveis com o Vim.
- Neovim Estrutura estrutura seu diretório de configuração
  - Segue a variável XDG_CONFIG_HOME que geralmente é em ~/.config/nvim
- Suporte Language Server Protocol (LSP) que permite auto complete de códigos e dicas de linguagens.
- Configuração padrão melhor e compatível com a do nvim.
- Melhor interface de usuário.

Uma parte interessante é que podemos apenas usar o mesmo arquivo de configuração apenas conferindo se é o vim ou o nvim (neovim)

Separei esta parte da documentação dentro da pasta [nvim](./nvim/)

```bash
# Fazendo um link para o novo arquivo de configuração 
ln -s ~/.vimrc ~/.config/nvim/init.vim

# dentro de init.vim

if has('nvim')
    " NeoVim specific commands
else
    " Standard Vim specific commands
endif
```

Instalar o neovim é simples também




## Extra

Site para aprender vim jogando
<https://vim-adventures.com/>

# Normal

No modo normal ou modo de navegação podemos manipular o texto sem necessáriamente fazer inserção de caracteres.
Também podemos navegar pelo texto de modo rápido.

Alguns detalhes mostram onde vc está no momento.

![](./pics/detalhes.png)

Todos os comandos baixo estaremos em modo de navegação, cada vez que entrar no modo de inserção é só para mostrar onde foi parar o comando.

>`ESC` é a tecla que sempre volta para o modo normal do Vim.

## Navagando entre as linhas

Para trabalhar com linhas geralmente se usa g.

### Ctrl u (up) ou Ctrl d (down)

Subir e descer o texto assim como em outros editores usando page up e page down é só utilizar esses comandos.

![updown](./gifs/normal_up_down.gif)

### gg (duas vezes a tecla g)

Também é possível ir direto para uma linha.Pensa no g como `go`
>gg = go go

Esse atalho no modo normal vai para a primeira linha

### G (shift G)

Esse atalho vai para a ultima. Lembra do antagônismo que falei antes?

![g](./gifs/normal_gg.gif)

>Já observe que digitar 50% foi direto pro meio do texto logo já fica uma dica ai. Assim como dá para ir pro topo com 1% e pro fim com 100%.

### [numero da linha](G ou shift g)

Se vc quiser ir para uma linha específica só falar o numero da linha seguido de G.

Existe uma configuração no vim que é muito interessante para mostrar o numero da linha. Mais pra frente vou mostrar algumas personalizações do Vim de forma fixa. por enquanto só digitar `:set number`. Para retirar `:set nonumber` .

Também da para ir para o número da linha como o modo de comando apertando :numerolinha porém se vc tem que apertar o : .

![g](./gifs/linego.gif)

## trabalhando com palavras (words)

Para trabalhar com palavras precisamos entender o que o vim considera uma quebra de palavra.
Quando pulamos palavras com o `w`ele considera todas as quebras de palavras, mas com o `W (shift W)` ele só considera os espaços.

As quebras de palavra default do vim são:
Um espaço é uma quebra de palavra
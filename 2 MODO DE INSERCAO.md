# Modo de inserção

Devemos aprender a entrar no modo de inserção do jeito certo para ganhar produtividade.

Entrar no modo de inserção poderia ser apertando a tecla `i` e navegar com a seta até onde quisermos, porém isso demora e não gera velocidade. Se vc esta aprendendo o Vim é pq vc quer velocidade e produtividade.

## Pensa no i como inserir ANTES

### i (insert)
Para entrar no modo de inserção `antes do cursor`
### I 
Vai para `ante de tudo`ou seja, inicio da linha
![](./gifs/insert_i.gif)

##Pensa no a como inserir DEPOIS
### a (append)
Para entrar `depois do cursor`
### A (append)
Para entrar `depois de tudo`, ou seja, final da linha

![](./gifs/insert_a.gif)

## Pensa no `o`como nova linha 

### o 
Em qualquer ponto que vc estiver ele vai abrir uma `nova linha abaixo` já no modo de inserção.

### O 
Em qualquer ponto que vc estiver ele vai abrir uma `nova linha acima` já no modo de inserção.

![](./gifs/insert_o.gif)

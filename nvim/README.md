# NeoVim

<https://github.com/neovim/neovim>

Para instalar tem várias maneiras dependendo do seu sistema operacional. A [documentação oficial de instalação](https://github.com/neovim/neovim/wiki/Installing-Neovim) pode te ajudar melhor caso precise

```bash
# Base debian
sudo apt-get install neovim
# Base arch
sudo pacman -S neovim
# Base Red Hat
sudo yum install -y neovim
```

Algums plugins necessitam de alguns pré requisitos instalados. Os que irei mostrar aqui precisam do python e do nodejs. Outros inclusive podem ser necessśario dependendo dos plugins que serão instalados.

No caso do python também é necessário do pip. Então garanta que tenha o python3 e o pip3 instalado.

``` bash
# Por exemplo no ubuntu
sudo apt-get install python3 python3-pip

# No arch
sudo pacman -S python3 python-pip

# O importante é isso esta funcionando

pip3 --version
pip 23.0.1 from /usr/lib/python3.10/site-packages/pip (python 3.10)

python --version
Python 3.10.10
```

Se quiser pode conferir na documentação [oficial do python](https://www.python.org/downloads/)

Do mesmo modo precisamos do [nodejs](https://nodejs.org/en/download) funcionando

```bash
# Ubuntu 
sudo apt install nodejs
# Arch 
sudo pacman -S nodejs
# O importante é estar funcionando
node --version
v19.8.1
```

## Terminal

Como o NeoVim irá rodar em um terminal, quanto melhor configurado ele estiver, com cores, fontes, mais bonito ficará a interface de trabalho.

Algumas sugestões..

Para shell:

- zsh
- fish

Para terminal:

- alacritty
- tilix
- terminator
- windows terminal (em caso de estar usando rodando em wls)

## Alias

Configure um alias, no seu terminal para v = nvim. Dessa forma ao digitar v irá chamar o nvim, ou vim ="nvim".

```bash
echo 'alias v="nvim"' >> ~/.zshrc
echo 'alias v="nvim"' >> ~/.bashrc
```

## Diretório de configuração

O nvim irá procurar o arquivo `vim.init` em `~/.config/nvim`

Vamos criar.

```bash
mkdir ~/.config/nvim
touch ~/.config/nvim/init.vim
```

Toda a configuração e customização será colocada dentro de `init.vim`.

## Instalador de plugins

Assim como o vscode o neovim tem vários plugins que podem extender o neovim. O jeito mais fácil de instalar os plugins é utilizar gerenciador de plugins. É possível fazer tudo manual, mas pra que fazer isso?

Existe vários... Escolha 1 e vai ser feliz.

- [vim-plug](https://github.com/junegunn/vim-plug)
- [Vundle](https://github.com/VundleVim/Vundle.vim)
- [Dein](https://github.com/Shougo/dein.vim)
- [Packer](https://github.com/wbthomason/packer.nvim)

Vamos utilizar o vim-plug que é fácil e funciona. O packer é mais interessante pelas minhas pesquisas, mas um pouco mais complicado de configurar. Deixe isso para quando tudo estiver funcionando e já estiver a mil por hora.

Simplesmente depois de instalado colocamos essas linhas no arquivo de configuração `init.vim`, sendo que no meio irá adicionar todos os plugins que será instalado. Mole não?

```bash
call plug#begin()
plug 'github do plugin'
plug 'github do outro plugin'
call plug#end()
```

No Repositório tem a instalação, mas no caso do linux só rodar o comando abaixo.

```bash
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

Este comando irá criar a pasta `~/.local/share/nvim` onde será administrado todos os plugins. Depois esse diretório irá crescer...

```bash
tree ~/.local/share/nvim   
/home/david/.local/share/nvim
└── site
    └── autoload
        └── plug.vim

3 directories, 1 file
```
